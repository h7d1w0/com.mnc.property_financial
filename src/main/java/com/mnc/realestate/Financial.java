package com.mnc.realestate;

public class Financial {
	private double propertyTax;
	private double insurance;
	private double other;
	private double income;
	public Financial(double propertyTax, double insurance, double other, double income) {
		super();
		this.propertyTax = propertyTax;
		this.insurance = insurance;
		this.other = other;
		this.income = income;
	}
	public double getPropertyTax() {
		return propertyTax;
	}
	public double getInsurance() {
		return insurance;
	}
	public double getOther() {
		return other;
	}
	public double getIncome() {
		return income;
	}
}
