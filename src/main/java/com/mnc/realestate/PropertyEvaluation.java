package com.mnc.realestate;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;


public class PropertyEvaluation {
	private Mortgage mortgage;
	private Financial financial;
	private double downpayment;
	private double downpaymentCost;
	private String propertyName = "";
	public PropertyEvaluation(Mortgage mortgage, Financial financial, double downpayment) {
		super();
		this.mortgage = mortgage;
		this.financial = financial;
		this.downpayment = downpayment;
	}
	
	public PropertyEvaluation(String propFileName) {
		super();
		Properties prop = new Properties();
	    InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

	    try {
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
	
			// get the property value and print it out
			propertyName = prop.getProperty("name");
			mortgage = new Mortgage(Double.parseDouble(prop.getProperty("mortgage.amount")),
					Integer.parseInt(prop.getProperty("mortgage.amortizationPeriod")),
					Double.parseDouble(prop.getProperty("mortgage.interest")), 
					prop.getProperty("mortgage.startDate"), Mortgage.convertPaymentFrequency(prop.getProperty("mortgage.paymentFrequency")));
			mortgage.updateInterestRate(convertPropertyStringToMap(prop, "mortgage.rateUpdates"));
			mortgage.addLumpsum(convertPropertyStringToMap(prop, "mortgage.lumpsum"));
			financial = new Financial(Double.parseDouble(prop.getProperty("financial.propertyTax")),
					Double.parseDouble(prop.getProperty("financial.insurance")),
					Double.parseDouble(prop.getProperty("financial.other")),
					Double.parseDouble(prop.getProperty("financial.income")));
			downpayment = Double.parseDouble(prop.getProperty("mortgage.downpayment"));
			downpaymentCost = downpayment * (mortgage.calInstallmentInterestRate(Double.parseDouble(prop.getProperty("financial.downpaymentInterest"))) - 1);
	    } catch (Exception e){
	    	System.out.println(e);
	    }

	}	

	public String getPropertyName() {
		return propertyName;
	}
	
	public List<Map<EvalutionMetrics, String>> calFinacialEvaluationMetrics() {
		List<Map<EvalutionMetrics, String>> metrics = new ArrayList<Map<EvalutionMetrics, String>>();
		int n = mortgage.getAmortizationPeriod();
		double runningTotalLumpsum = 0;
		double runningTotalInterest = 0;
		double runningTotalPrincipal = 0;
		double runningTotalCashFlow = 0;
		double runningTotalProfit = 0;
		double runningTotalAddon = 0;
		for(int i = 0; i < n; i++) {
			Map<EvalutionMetrics, String> metric = new HashMap<>();
			metric.put(EvalutionMetrics.PAYMENT_SEQ, String.format("%03d", i+1));
			metric.put(EvalutionMetrics.PAYMENT_DATE, mortgage.getPaymentDate(i).toString());
			runningTotalLumpsum += mortgage.getLumpsum(i);
			runningTotalInterest += mortgage.getInterest(i);
			runningTotalPrincipal += mortgage.getPrincipal(i);
			double operationCost = financial.getInsurance() + financial.getPropertyTax() + financial.getOther();
			double cashflow = financial.getIncome() - operationCost -
					mortgage.getInterest(i) - mortgage.getPrincipal(i) - downpaymentCost;
			double profit = cashflow + mortgage.getPrincipal(i);
			runningTotalProfit += profit;
			runningTotalAddon += cashflow < 0 ? -cashflow : 0;
			double equityEfficiency = mortgage.convertToActualYearlyRate(profit / (this.downpayment + runningTotalAddon + runningTotalLumpsum)) * 100;
			double downpaymentProfit = (financial.getIncome() - operationCost) * this.downpayment / (this.downpayment + mortgage.getOriginalAmount());
			double downpaymentEfficiency = mortgage.convertToActualYearlyRate(downpaymentProfit / this.downpayment) * 100;
			runningTotalCashFlow += cashflow;
			
			metric.put(EvalutionMetrics.INTEREST, String.format("%,.2f", mortgage.getInterest(i)));
			metric.put(EvalutionMetrics.PRINCIPAL, String.format("%,.2f", mortgage.getPrincipal(i)));
			metric.put(EvalutionMetrics.INSTALLMENT, String.format("%,.2f", mortgage.getInterest(i) + mortgage.getPrincipal(i)));
			metric.put(EvalutionMetrics.CASH_FLOW, String.format("%,.2f", cashflow));
			metric.put(EvalutionMetrics.UNREALIZED_PROFIT, String.format("%,.2f", profit));
			metric.put(EvalutionMetrics.ACCUMULATED_INTEREST, String.format("%,.2f", runningTotalInterest));
			metric.put(EvalutionMetrics.ACCUMULATED_PRINCIPAL, String.format("%,.2f", runningTotalPrincipal + runningTotalLumpsum));
			metric.put(EvalutionMetrics.ACCUMULATED_CASH_FLOW, String.format("%,.2f", runningTotalCashFlow));
			metric.put(EvalutionMetrics.ACCUMULATED_PROFIT, String.format("%,.2f", runningTotalProfit));
			metric.put(EvalutionMetrics.UNREALIZED_EQUITY_EFFICIENCY, String.format("%,.2f", equityEfficiency));
			metric.put(EvalutionMetrics.DOWNPAYMENT_EFFICIENCY, String.format("%,.2f", downpaymentEfficiency));
			metric.put(EvalutionMetrics.DOWPAYMENT_PROFIT, String.format("%,.2f", downpaymentProfit));
			metrics.add(metric);
		}
		return metrics;
	}
	
	public void showFinacialEvaluation(int n) {
		Installment[] in = mortgage.getInstallments();
		double[] lumpsums = mortgage.getLumpsumSchedule();
		String sep = "   ";
		String header = " " + sep + "Date" + sep + "Interest" + sep + "Principal" + sep + "CashFlow" + sep + "Profit" + sep + "Efficiency%" + sep + "TotalInterest";
		System.out.println(header);
		if (n > mortgage.getAmortizationPeriod()) {
			n = mortgage.getAmortizationPeriod();
		}
		double runningTotalLumpsum = 0;
		double runningTotalInterest = 0;
		for(int i = 0; i < n; i++) {
			runningTotalLumpsum += lumpsums[i];
			runningTotalInterest += in[i].getInterest();
			double cashflow = financial.getIncome() - financial.getInsurance() - 
					financial.getPropertyTax() - financial.getOther() -
					in[i].getInterest() - downpaymentCost - in[i].getPrincipal();
			double profit = cashflow + in[i].getPrincipal();
			double downpaymentEfficiency = mortgage.convertToActualYearlyRate(profit / (downpayment + runningTotalLumpsum)) * 100;
			System.out.print(in[i].getNumberOfInstallment() + sep);
			System.out.print(mortgage.getPaymentDate(i) + sep);
			System.out.print(String.format("%,.2f", in[i].getInterest()) + sep);
			System.out.print(String.format("%,.2f", in[i].getPrincipal()) + sep);
			System.out.print(String.format("%,.2f", cashflow) +  sep);
			System.out.print(String.format("%,.2f", profit) +  sep);
			System.out.print(String.format("%,.2f", downpaymentEfficiency) +  sep);
			System.out.print(String.format("%,.2f", runningTotalInterest) +  sep);
			System.out.println();
			if ((i +  1) % 12 == 0 ) {
				System.out.println(header);
			}
		}
	}
	
	public void showDownpaymentEvaluation() {
		Installment[] in = mortgage.getInstallments();
		double ratio = downpayment / mortgage.getOriginalAmount();
		String sep = "   ";
		String header = " " + sep + "Income" + sep + "Cost" + sep  + "Profit" + sep + "Profit%";
		System.out.println(header);
		
		for(int i = 0; i < mortgage.getAmortizationPeriod(); i++) {
			double cost = (financial.getInsurance() + 
					financial.getPropertyTax() + financial.getOther() + in[i].getInterest()) * ratio;
			System.out.print(in[i].getNumberOfInstallment() + sep);
			System.out.print(String.format("%,.2f", financial.getIncome() * ratio) + sep);
			System.out.print(String.format("%,.2f", cost * ratio) + sep);
			System.out.print(String.format("%,.2f", (financial.getIncome() - cost) * ratio) +  sep);
			System.out.print(String.format("%,.2f", mortgage.convertToActualYearlyRate((financial.getIncome() - cost) * ratio / downpayment) * 100) +  sep);
			System.out.println();
			if ((i +  1) % 12 == 0 && i < mortgage.getAmortizationPeriod() - 1) {
				System.out.println(header);
			}
		}
	}
	
	private Map<Integer, Double> convertPropertyStringToMap(Properties prop, String property) {
		Map<Integer, Double> propertyValues = new HashMap<>();
		String propertyValue = prop.getProperty(property);
		if (!propertyValue.isBlank()) {
			String[] propertyValueArray = propertyValue.split("\\|");
			for(int i = 0; i < propertyValueArray.length; i++) {
				String[] pair = propertyValueArray[i].split(",");
				propertyValues.put(Integer.parseInt(pair[0]), Double.parseDouble(pair[1]));
			}
		}
		return propertyValues;
	}
	
	public static enum EvalutionMetrics {
		PAYMENT_SEQ,
		PAYMENT_DATE,
		INTEREST,
		PRINCIPAL,
		INSTALLMENT,
		CASH_FLOW,
		UNREALIZED_PROFIT,
		ACCUMULATED_INTEREST,
		ACCUMULATED_PRINCIPAL,
		ACCUMULATED_INSTALLMENT,
		ACCUMULATED_CASH_FLOW,
		ACCUMULATED_PROFIT,
		UNREALIZED_EQUITY_EFFICIENCY,//EQUITY = DOWNPAYMENT + FUND to COVER NEGATIVE CASH FLOW
		DOWNPAYMENT_EFFICIENCY,// INCOME PRO-RATED TO DOWNPAYMENT not RE-INVESTED
		DOWPAYMENT_PROFIT// INCOME PRO-RATED TO DOWNPAYMENT
	}
}
