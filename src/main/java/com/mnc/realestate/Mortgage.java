package com.mnc.realestate;

import java.time.LocalDate;
import java.util.Map;
import java.util.TreeMap;

public class Mortgage {
	// originalAmount
	private LocalDate startDate;
	private double V0;
	private int originalAmortizationPeriod;
	private double interestRate;
	private PaymentFrequency paymentFreq = PaymentFrequency.MONTHLY;
	// Below are derived fields
	// total number Of Installment
	private int N;
	// installmentInterestRateFactor: interest rate + 1
	private double R;
	// installmentAmount
	private double P;
	private double[] interestRateSchedule;
	private double[] lumpsumSchedule;
	private double[] accelerateRateSchedule;
	private Installment[] installmentSchedule;
	
	public Mortgage(double originalAmount, int originalAmortizationPeriod, double interestRate, String startDate, PaymentFrequency paymentFreq) {
		super();
		this.V0 = originalAmount;
		this.originalAmortizationPeriod = originalAmortizationPeriod;
		this.interestRate = interestRate;
		this.startDate = LocalDate.parse(startDate).minusDays(1l).plusMonths(1l).withDayOfMonth(1);
		this.paymentFreq = paymentFreq;
		initialize();
		updateInstallmentSchedule(0);
	}

	private void calInstallment(int n) {
		if (n < 0 || n >= N) {
			return;
		}
		double paidInterest = getPrevOutstanding(n) * interestRateSchedule[n];
		double paidPrinciple = P * (1 + accelerateRateSchedule[n]) + lumpsumSchedule[n] - paidInterest;
		double outstanding = getPrevOutstanding(n) - paidPrinciple;
		if (outstanding < 0) {
			outstanding = 0;
			paidPrinciple = 0;
		}
		installmentSchedule[n] = new Installment(n, paidInterest, paidPrinciple, outstanding);
	}
	
	public void updateInstallmentSchedule(int n) {
		if (n < 0) n = 0;
		if (n > N - 1) n = N - 1;
		for (int i = n; i < N; i++) {
			calInstallment(i);
		}
	}
	
	private void initialize() {
		N = this.paymentFreq == PaymentFrequency.MONTHLY ? originalAmortizationPeriod * 
				12 : originalAmortizationPeriod * 26;
	    R = calInstallmentInterestRate(this.interestRate);
	    double RT = Math.pow(R, N);
		P = RT * (R - 1) * V0 / (RT - 1);
		interestRateSchedule = new double[N];
		lumpsumSchedule = new double[N];
		accelerateRateSchedule = new double[N];
		installmentSchedule = new Installment[N];
		for (int i = 0; i < N; i++) {
			interestRateSchedule[i] = R - 1;
			lumpsumSchedule[i] = 0.0;
			accelerateRateSchedule[i] = 0.0;
		}
	}

	public double calInstallmentInterestRate(double nominalRate) {
		int n = this.paymentFreq == PaymentFrequency.MONTHLY ? 6 : 13;
		return Math.pow(1 + 0.5 * nominalRate, 1.0/n);
	}
	
	public double convertToActualYearlyRate(double installmentRate) {
		if (this.paymentFreq == PaymentFrequency.MONTHLY) {
			return Math.pow(1 + installmentRate, 12) - 1;
		} else {
			return Math.pow(1 + installmentRate, 26) - 1;
		}
	}
	
	public double[] getInterestRateSchedule() {
		return interestRateSchedule;
	}

	public void updateInterestRate(int n, double newInterestRate) {
		double newRate = calInstallmentInterestRate(newInterestRate);
		for(int i = n; i < N; i++) {
			this.interestRateSchedule[i] = newRate;
		}
		updateInstallmentSchedule(n);
	}

	public void updateInterestRate(Map<Integer, Double> newInterestRates) {
		if (!newInterestRates.isEmpty()) {
			TreeMap<Integer, Double> sortedNewRates = new TreeMap<>(newInterestRates);
			for(int key: sortedNewRates.keySet()) {
				double newRate = calInstallmentInterestRate(sortedNewRates.get(key)) - 1;
				for(int i = key - 1; i < N; i++) {
					this.interestRateSchedule[i] = newRate;
				}
			}
			updateInstallmentSchedule(sortedNewRates.firstKey());
		}
	}
	
	public double[] getLumpsumSchedule() {
		return lumpsumSchedule;
	}

	public void addLumpsum(int n, double lumpsum) {
		if (n<0) {
			n = 0;
		} else if (n >= N) {
			n = N - 1;
		}
		this.lumpsumSchedule[n] = lumpsum;
		updateInstallmentSchedule(n);
	}
	
	public void addLumpsum(Map<Integer, Double> lumpsums) {
		if (!lumpsums.isEmpty()) {
			TreeMap<Integer, Double> sortedLumpsums = new TreeMap<>(lumpsums);
			for(int key: sortedLumpsums.keySet()) {
				double lumpsum = sortedLumpsums.get(key);
				this.lumpsumSchedule[key - 1] = lumpsum;
			}
			updateInstallmentSchedule(sortedLumpsums.firstKey());
		}
	}
	
	public double[] getAccelerateRateSchedule() {
		return accelerateRateSchedule;
	}

	public void setAccelerateRate(int n, double rate) {
		for(int i = n - 1; i < N; i++) {
			this.accelerateRateSchedule[i] = rate;
		}
	}
	
	private double getPrevOutstanding(int n) {
		if (n <= 0) {
			return V0;
		} else if (n < N && n >= 1) {
			return installmentSchedule[n - 1].getOutstanding();
		} else {
			return 0.0;
		}
	}
	
	public LocalDate getStartDate() {
		return startDate;
	}

	public Installment[] getInstallments() {
		return installmentSchedule;
	}
	
	public double getInterest(int n) {
		return this.installmentSchedule[n].getInterest();
	}
	
	public double getPrincipal(int n) {
		return this.installmentSchedule[n].getPrincipal();
	}
	
	public double getLumpsum(int n) {
		return this.lumpsumSchedule[n];
	}
	
	public int getTerm() {
		return 5;
	}
	
	public int getAmortizationPeriod() {
		return N;
	}

	public double getOriginalAmount() {
		return V0;
	}
	
	public LocalDate getPaymentDate(int numberOfInstallment) {
		if (this.paymentFreq == PaymentFrequency.MONTHLY) {
			return startDate.plusMonths(numberOfInstallment + 1);
		} else {
			return startDate.plusWeeks((numberOfInstallment + 1) * 2);
		}
	}
	
	public static PaymentFrequency convertPaymentFrequency(String freq) {
		if (freq.toUpperCase().equals("BIWEEKLY")) {
			return PaymentFrequency.BIWEEKLY;
		} else {
			return PaymentFrequency.MONTHLY;
		}
	}
	
	public static enum PaymentFrequency{
		MONTHLY,
		BIWEEKLY
	}
}
