package com.mnc.realestate;

public class Installment {
	private int numberOfInstallment;
	private double interest;
	private double principal;
	private double outstanding;
	public Installment(int numberOfInstallment, double interest, double principal, double outstanding) {
		super();
		this.numberOfInstallment = numberOfInstallment;
		this.interest = interest;
		this.principal = principal;
		this.outstanding = outstanding;
	}
	public int getNumberOfInstallment() {
		return numberOfInstallment;
	}
	public double getInterest() {
		return interest;
	}
	public double getPrincipal() {
		return principal;
	}
	public double getOutstanding() {
		return outstanding;
	}
}
