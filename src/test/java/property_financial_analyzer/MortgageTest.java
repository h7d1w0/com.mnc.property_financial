package property_financial_analyzer;


import static org.assertj.core.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.mnc.realestate.Mortgage;
import com.mnc.realestate.Mortgage.PaymentFrequency;


public class MortgageTest {
	
	private static Mortgage mortgage;
	//Mortgage amount
	private static double V0 = 1000000;
	//Amortization period
	private static int originalAmortizationPeriod = 30;
	//Nominal annual interest rate
	private static double interestRate = 0.0135;
	//actual interest rate for each installment
	private static double R;
	//payment of each installment
	private static double P;
	
	@BeforeAll
	static void setup() {
		mortgage = new Mortgage(V0, originalAmortizationPeriod, interestRate, "2021-08-09", PaymentFrequency.MONTHLY);
	}
	
    @Test 
    public void actualRate_correct() {
       double actualRate = mortgage.calInstallmentInterestRate(interestRate);
       assertThat(actualRate).isEqualTo(1.0011218489271754, withPrecision(10d));
    }
    
    @Test 
    public void mortgageStartDate_useFirstDateOfNextMonth() {
       assertThat(mortgage.getStartDate()).isEqualTo(LocalDate.parse("2021-09-01"));
    }
    
    @Test 
    public void mortgageStartDate_is_first_day_of_month() {
       Mortgage mortgage = new Mortgage(V0, originalAmortizationPeriod, interestRate, "2021-10-01", PaymentFrequency.MONTHLY);
       assertThat(mortgage.getStartDate()).isEqualTo(LocalDate.parse("2021-10-01"));
    }
    
    @Test 
    public void mortgageStartDate_is_first_day_of_month2() {
       Mortgage mortgage = new Mortgage(V0, originalAmortizationPeriod, interestRate, "2022-01-01", PaymentFrequency.MONTHLY);
       assertThat(mortgage.getStartDate()).isEqualTo(LocalDate.parse("2022-01-01"));
    }
    
//	private double calculateAccumulatedInterest(int n, double R, double V0, double P) {
//		double RN = Math.pow(R, n);
//		double nthAccumulatedInterest = ((R - 1) * V0 - P) * (RN - 1) / (R - 1) + n * P;
//        return nthAccumulatedInterest;
//	}
//	private double calculateNthInterest(int n, double R, double V0, double P) {
//		double RN = Math.pow(R, n);
//		double nthInterest = calculateNthInstallmentRemain(n, R, V0, P) * (R - 1);
//        return nthInterest;
//	}
//	private double calculatAccumulatedPrinciple(int n, double R, double V0, double P) {
//		double RN = Math.pow(R, n);
//		double nthAccumulatedInstallment = n * P;
//		double nthAccumulatedInterest = ((R - 1) * V0 - P) * (RN - 1) / (R - 1) + n * P;
//		double nthAccumulatedPrinciple = nthAccumulatedInstallment - nthAccumulatedInterest;
//        return nthAccumulatedPrinciple;
//	}	
//	
//	private double calculateNthInstallmentRemain(int n, double R, double V0, double P) {
//		double RN = Math.pow(R, n);
//		double nthRemain = RN * V0 - P * (RN - 1) / (R - 1);
//		return nthRemain;
//	}
}
