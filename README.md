# Real Estate Property Financial Analyzer Project
This project is created for real estate property investors to evaluate a property from the financial perspective.  This 
project contains the core functions to analyze a property financially.  Other projects that have enhanced user interface
will be using the classes in this project: a REST API might be created later.

# Use
As a independant project, users can add new property information to java property file in the resource directory to obtain
the financial evaluation result.
